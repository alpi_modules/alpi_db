var my = require('mysql');

module.exports = class Database {
    /**
     * Creates an instance of Database.
     * 
     * @param {String[]} connectionInformations [host, user, password, database]
     */
    constructor(connectionInformations) {
        this.connection = my.createConnection({
            host: connectionInformations[0],
            user: connectionInformations[1],
            password: connectionInformations[2],
            database: connectionInformations[3]
        });
        this.connection.connect();
    }

    /**
     * SELECT request
     * 
     * @param {String} request
     * @param {(Boolean|Object)} callback
     */
    SelectRequest(request, callback) {
        this.connection.query(request, (err, rows, fields) => {
            if (!err) {
                callback({
                    rows: rows,
                    fields: fields,
                    request: request
                });
            } else {
                callback(false);
            }
        });
    }

    /**
     * Prepared SELECT request
     * 
     * 
     * @param {String} request 
     * @param {Array} placeholders For tables or rows surround the placeholder with "`"
     * @param {(Boolean|Object)} callback 
     */
    SelectPreparedRequest(request, placeholders, callback) {
        var preparedRequest = this._preparedRequestProcessor(request, placeholders);
        if (typeof preparedRequest !== "boolean") {
            this.SelectRequest(preparedRequest, (result) => {
                callback(result);
            });
        } else {
            callback(false);
        }
    }

    /**
     * Query request
     * 
     * @param {String} request 
     * @param {(Boolean|Object)} callback 
     */
    QueryRequest(request, callback) {
        this.connection.query(request, (err, results, fields) => {
            if (!err) {
                callback({
                    results: results,
                    fields: fields,
                    request: request
                });
            } else {
                callback(false);
            }
        });
    }

    /**
     * Prepared Query request
     * 
     * @param {String} request 
     * @param {Array} placeholders 
     * @param {(Boolean|Object)} callback 
     */
    QueryPreparedRequest(request, placeholders, callback) {
        var preparedRequest = this._preparedRequestProcessor(request, placeholders);
        if (typeof preparedRequest !== "boolean") {
            this.QueryRequest(preparedRequest, (result) => {
                callback(result);
            });
        } else {
            callback(false);
        }
    }

    /**
     * End the database connection
     * 
     */
    end() {
        this.connection.end();
    }

    /**
     * Prepared request processor
     * 
     * @param {String} request 
     * @param {Array} placeholders 
     * @returns {(Boolean|String)} 
     */
    _preparedRequestProcessor(request, placeholders) {
        var requestArray = request.split("?");
        if (placeholders.length > 0 && requestArray.length == (placeholders.length + 1)) {
            var preparedRequest = requestArray[0];
            for (var i = 1; i < requestArray.length; i++) {
                var placeholder = placeholders[i - 1];
                if (typeof placeholder == "string") {
                    if (placeholder.indexOf("`") == 0 && placeholder.lastIndexOf("`") == placeholder.length - 1) {
                        preparedRequest += placeholder;
                    } else {
                        preparedRequest += '"' + placeholder + '"';
                    }
                } else if (typeof placeholder == "number") {
                    preparedRequest += placeholder;
                } else if (typeof placeholder == "boolean") {
                    if (placeholder == true) {
                        preparedRequest += "TRUE";
                    } else {
                        preparedRequest += "FALSE";
                    }
                } else {
                    return false;
                }
                preparedRequest += requestArray[i];
            }
            return preparedRequest;
        } else {
            return false;
        }
    }
};